# StrutsWebApp



### From Tutorial : 
http://www.javawebtutor.com/articles/maven/maven_struts_example.php

### Source Code
http://www.javawebtutor.com/code/maven/StrutsWebApp.rar


### Tomcat
Landing Page : https://tomcat.apache.org/download-80.cgi
Installer : https://downloads.apache.org/tomcat/tomcat-8/v8.5.54/bin/apache-tomcat-8.5.54.zip

### Running Tomcat
1. Copy the WAR file you have just created to CATALINA_HOME/webapps, e.g., c:/Tomcat8/webapps.
2. Start the Tomcat server. e.g., catalina.bat run
3. In the address area of the browser, type `http://localhost:8090/StrutsLoginExample` and submit that address to the browser.


